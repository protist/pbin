pbin
=====
Personal bin (i.e. not related to bioinformatics). A random assortment of
scripts that others may find useful.

bu.sh
-----
Activate this script with a udev rule to automatically trigger a back up with
[backintime](http://backintime.le-web.org/) when a specific hard drive is
attached. Since you might not want to backup every time you connect the hard
drive, a dialogue box allows you to cancel backup (or automatically trigger
backup if there is no input).

tm900_transfer_videos.sh
------------------------
Automatically copies videos from an attached Panasonic TM900. Probably works for
most models of video or still cameras, with modification of the input path.
Since the default filenames are uninformative, also renames the files according
to the date and time, as per the EXIF data. Requires exiftool.
