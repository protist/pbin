#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copy video files from a Panasonic TM900, and rename them based on their date and time (from their EXIF data).

SOURCEDIR=/media/CAM_MEM/AVCHD/BDMV/STREAM/
DESTDIR=~/HDD/Personal/Videos/TM900/

cd $SOURCEDIR
for file in `ls .`; do
  if [[ $file =~ [0-9]{5}\.MTS ]]; then
    TIMESTAMP=$(exiftool ${file} | grep 'Date/Time Original' | awk '{print $4 $5}' | tr -d : | sed -E 's/^[0-9]{2}([0-9]{6})([0-9]{6})\+[0-9]{4}/\1.\2/')
    cp -npv $file "${DESTDIR}${TIMESTAMP}.${file}"
  else
    echo "Error, ${file} is not [0-9]{5}.MTS"
  fi
done
xdg-open $DESTDIR
