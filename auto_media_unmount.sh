#!/usr/bin/env bash
# Search for mounted media, then ask to unmount each.
# Written for a Raspbian Raspberry Pi.
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

media_mount_list="$(mount | grep 'on /media/')"
non_fstab_mount_list=$(grep -v '/dev/sd[ab]1 on /media/PiHDD' <<< "$media_mount_list")

# read from a different file descriptor (3), so that nested read can read from stdin.
if [[ -z $non_fstab_mount_list ]]; then
  echo Nothing to unmount!
else
  while read -r -u 3 mount_line ; do
    mount_path=$(cut -d " " -f 3 <<< $mount_line)
    read -p "Attempt to unmount $mount_path (y/N)?" response
    if [[ $response == 'y' ]] ; then
      sudo umount "$mount_path" && sudo rmdir "$mount_path"
      echo "$mount_path unmounted"
    fi
  done 3<<< "$non_fstab_mount_list"
fi
