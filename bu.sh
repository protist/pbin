#!/usr/bin/env bash
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script is intended to be run by root by udev, but it might still work
#   when called directly by the user. I left the "sudo" parts here (including
#   the sudo_me loop), just in case it will work normally.
# The script checks if the right drive is there (as udev only matches device
#   model), asks user to begin, runs backintime, then "safely remove" disk (i.e.
#   unmount and power down).
# It also reports on how long the backup took.
# I've removed the part of the script preventing suspension, since this has
#   variable support depending on desktop environments. Instead, install
#   caffeine and automatically activate for "backintime".
# To use, replace the three parts of the script between <triangular brackets>,
#   install at /usr/local/bin, and create the relevant udev rules, e.g. at
#   /etc/udev/rules.d/02-backup-automatically.rules
# # UDEV rules for Western Digital MyBook.
# # to get serial number, use one of
# # udevadm info -a -p /sys/block/sdc/sdc1
# # udevadm info -a -p $(udevadm info -q path -n /dev/sdc)
# ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="<replace>", ATTR{idProduct}=="<replace>", ATTRS{serial}=="<replace>" RUN+="/usr/local/bin/bu"

CURRENTUSER=<user>
# Define the UUIDs for the LUKS crypt (e.g. dm-0) and for the drive itself (e.g. sdb, sdb1).
# You can find UUIDs with `blkid` or `ls -l /dev/disk/by-uuid/`
LUKSUUID=<a long string, e.g. 36 characters>
HDUUID=<a long string, e.g. 36 characters>

# Prevent sudo from timing out. Adopted from http://serverfault.com/questions/266039/temporarlly-increasing-sudos-timeout-for-the-duration-of-an-install-script I didn't understand the details of trap, so i removed it.
sudo_done="/tmp/backintimefinished"
sudo_me() {
 while [ ! -f $sudo_done ]; do
  sudo -v
  sleep 5
 done &
}

stop_sudoing() {
  touch $sudo_done
  sleep 7
  rm $sudo_done
}

# Have to put it in a subroutine, otherwise udev waits until the script is done before mounting it.
scriptproper () {
# Need this for zenity (and guake?).
export DISPLAY=:0.0

# Wait until the drive is mounted. N.B. this just tests if the volume is unlocked, not necessarily mounted.
# Sleep an extra few seconds after the zenity question, as it seems to take 12-21 seconds before it's mounted.
notmounted=true
elapsed=0
while [ $notmounted ]; do
  if [[ -e /dev/disk/by-uuid/${LUKSUUID} ]]; then
    notmounted=$false
  fi
  if [[ $elapsed == 60 ]]; then
    sudo -u $CURRENTUSER zenity --error --title "Backup" --text "Error. LUKS crypt not found within $elapsed seconds."
    exit
  else
    sleep 1
    let elapsed++
  fi
done

# Ask user if ready to backup. N.B. 0 for Cancel and 1 for OK, which is counterintuitive, but necessary to make "OK" default (and 70 for timeout).
sudo -u $CURRENTUSER yad --title=Backup --text="Backup will commence in 5 seconds." --timeout=5 --button=Cancel:0 --button=OK:1 --image "dialog-question" --timeout-indicator=bottom --sticky
answer=$?
if [[ $answer == 0 ]] || [[ $answer == 252 ]]; then # cancel or press esc
  exit
elif [[ $answer == 70 ]]; then # timeout
  sudo -u $CURRENTUSER xset dpms force off # i.e. turn off screen
fi

sleep 5 # allow time for the crypt to mount.

# Uncomment to allow the user to see the output/progress in guake.
# sudo -u $CURRENTUSER guake -n NEW_TAB -e "tail -f /tmp/backintime_stdout /tmp/backintime_stderr"

# Commence backup, and prevent sudo from timing out.
sudo -v
sudo_me &

# Start timer.
STARTTIME=`date +%s`

# Do the backup (alternatively, select profile with --profile), and then create the temp file.
sudo backintime -b
stop_sudoing &

# Find the diskid. e.g. dm-0 for LUKS crypt; sdb, sdc for drives
luksid=`ls -l /dev/disk/by-uuid/${LUKSUUID} | sed -r 's/^.* -> ..\/..\/(....)$/\1/'`
hdid=`ls -l /dev/disk/by-uuid/${HDUUID} | sed -r 's/^.* -> ..\/..\/(...).$/\1/'`

# Got an error a few times (Device /dev/dm-0 is busy.), so try sleeping for a few seconds. 10 seconds failed, so let's try 20.
sleep 20

# Unmount the LUKS partition, and the disk (and don't power it down).
udisks --unmount /dev/${luksid} && sleep 5 && cryptsetup luksClose /dev/${luksid}

# End timer.
ENDTIME=`date +%s`
TOTALSECONDS=`expr $ENDTIME - $STARTTIME`
TOTALTIME=`date -d "1970-01-01 ${TOTALSECONDS} sec" +'%k:%M:%S' | sed -r 's/^(0:(0)?)?//'`
echo "Backup complete. The backup took ${TOTALTIME}."

# Send a notification.
sudo -u $CURRENTUSER notify-send "Back In Time" "Backup is complete. The backup took ${TOTALTIME}." -i gtk-save --hint=int:transient:1

# Power down disk only when you know the user is there, since my WD will just power up again unless the cable is removed.
sleep 1 # So the notify-send has time to sink in, and you're not halfway typing.
if [[ `cat /tmp/backintime_stderr` = "" ]]; then
  # No errors.
  sudo -u $CURRENTUSER zenity --question --title "Backup" --text "The backup took ${TOTALTIME}. Eject disk?"
  if [[ $? == 1 ]]; then
    exit
  else
    udisks --detach /dev/${hdid}
  fi
else
  # Errors.
  sudo -u $CURRENTUSER zenity --error --title "Backup" --text "Warning: Back In Time reported errors! Opening standard error. (The backup took ${TOTALTIME}.)"
  sudo -i -u $CURRENTUSER xdg-open /tmp/backintime_stderr
fi
}

# Initialise (and clear) the files first, in case they have not been created (otherwise tail -f will fail).
echo "" > /tmp/backintime_stdout
echo "" > /tmp/backintime_stderr
scriptproper >> /tmp/backintime_stdout 2>> /tmp/backintime_stderr &
