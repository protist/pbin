#!/usr/bin/env bash
# Search for connected but unmounted media, then ask to mount each.
# Written for a Raspbian Raspberry Pi after automount was removed.
#
# Copyright 2013 Lee M. Yeoh (email: "plast-dot-id-dot-au" follows "gitlab")
# This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

USERUID=1000
USERGID=1000

# exclude e.g. /dev/mmcblk0p2, since it's not in `mount`
blkid_list="$(blkid | grep '^/dev/sd')"
mount_list="$(mount)"
mount_list_device_names="$(grep -o '^/dev/sd[^ ]*' <<< "$mount_list")"

# read from a different file descriptor (3), so that nested read can read from stdin.
while read -r -u 3 blkid_line ; do
  device_name=$(awk '{print $1}' <<< "$blkid_line"| sed 's/:$//') # e.g. /dev/sda1
  device_label=$(sed -nr 's/.*LABEL="([^"]*)".*/\1/p' <<< "$blkid_line") # empty if not set.
  [ -z $device_label ] && device_label=$(sed -nr 's,/dev/(.*),\1,p' <<<$device_name)
  # sanity check
  grep -q '/' <<< $device_label && echo ERROR: $device_label has a slash && exit 1

  if echo "$mount_list_device_names" | grep -q "^${device_name}$" ; then
    true # echo $device_name aka \"$device_label\" already mounted
  else
    read -p "Attempt to mount $device_name aka \"$device_label\"? (y/N)" response
    if [[ $response == 'y' ]] ; then
      sudo mkdir /media/$device_label
      sudo mount -o uid=$USERUID,gid=$USERGID,noatime $device_name /media/$device_label
      echo Device mounted at /media/$device_label
    fi
  fi
done 3<<< "$blkid_list"
